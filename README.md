#[rollingWolf / Random Stuff](https://bitbucket.org/rollingWolf/random-stuff/)
These files are just concepts and not created for use in production even though with some tweaking they should work without any problem. They are delivered as is though. This is old code Im offereing for people whos interested and there may be small parts missing but I hope that there shouldnt be too much problem coding the lines thats missing to get it to work with your code.

##RandomArt
Class to generate a SSH randomart ascii image from fingerprint.
```php
    $fingerprint = 'ab:cd';
    echo RandomArt::generate($fingerprint);

    // OUTPUT
    +--[ RSA 2048]----+
    |                 |
    |                 |
    |                 |
    |                 |
    |    E   S        |
    |     .   .       |
    |      . .        |
    |       =         |
    |      . o        |
    +-----------------+
```

##jsCDN
jscdn is a class that lets you define what js and/or css files from common libraries you want to use and return <script> and <link> tags to them hosted on [cdnjs.com] not endorsed in any way. The basic concept is something like this:
```php
    $files = array(
        'jquery' => array('version' => '2.1.0'),
        'jqueryui' => array(
            'version' => '1.10.4',
            'includeMatch' => array(
                'jquery-ui.min.css$'
            ),
        ),
        'local' => array(
            'js/main.js',
            'css/style.css',
            )
        );
    $settings = array(
        'cacheDir' => './cache',
        );
    $jsCDN = new jscdn($settings);
    $jsCDN->requireFiles($files); // download and parse json list from cdnjs.com
    $jsCDN->getIncludes('js|css'); // set which types of files to actually include
    foreach ($jsCDN->printIncludes as $include) {
        echo $include; // <script> or <link>
    }
```

##svg2png
I made this class to convert svg into png on the fly either as a CLI script or webpage. You can define parameters like transparent background, to change black to any other color etc. but other than that its pretty basic and was made for icons in svg form with just a background and black foreground.
```php
    $svg2png = new svg2png();
    $svg2png->setBG(); // set background to transparent
    $svg2png->load('icons/script.svg'); // load icons/script.svg
    $svg2png->setFG('orange');  // switch black foreground to orange
    $svg2png->setSize(256, 256); // output square image 256x256 pixels
    $svg2png->write('script.png'); // write into script.png
```

##utf8fixer
This is ust a bit of snippet I found in an old project where I scrape webpages and utf8 encoding was sometimes wrong. Either the webpage sent unencoded utf8 but claiming it was utf8 encoded or that whatever scraping method I used made the utf8 encoded text utf8 encoded again. It assumes that you dont know what youll get and just try and make sure it is properly utf8encoded regardless of what it was originally.
```php
    if(is_utf8($string)) {
        echo '$string is utf8 encoded';
    }
    /**
     * This is basically mb_check_encoding ( $string, 'UTF-8' ); but without any
     * dependencies.
     * Returns true if it can detect utf8 in string
     */
```

```php
    /**
     * fix_utf8 recursivle encodes or decodes string to valid utf8
     */
    define('DEBUG', true);
    $data = file_get_contents('testutf8.txt'); //file with utf8 chars
    $data = utf8_encode($data);
    $data = utf8_encode($data);
    $data = utf8_encode($data);
    $data = utf8_encode($data);
    $data = utf8_encode($data);
    echo '"'.fix_utf8($data).'"';
    echo PHP_EOL;
    /**
     * WARNING: if string has gone through more than 6 decodes, script
     * will loop forever
     */
    $data = utf8_decode($data);
    $data = utf8_decode($data);
    $data = utf8_decode($data);
    echo '"'.fix_utf8($data).'"';
```

```php
    /**
     * OUTPUT:
     *
     * hejsan pÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¥ dig dÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¤r
     * hejsan pÃÂÃÂÃÂÃÂ¥ dig dÃÂÃÂÃÂÃÂ¤r
     * hejsan pÃÂÃÂ¥ dig dÃÂÃÂ¤r
     * hejsan pÃÂ¥ dig dÃÂ¤r
     * hejsan pÃ¥ dig dÃ¤r
     * hejsan på dig där
     * ---- DONE "hejsan på dig där"
     * hejsan pÃÂ¥ dig dÃÂ¤r
     * hejsan pÃ¥ dig dÃ¤r
     * hejsan på dig där
     * ---- DONE "hejsan på dig där"
     *
     */
```

```php
    $string = remove_utf8_bom($string);
    /**
     * I love phpQuery. Problem with phpQuery is that sometimes a byte order mark
     * (bom) causes phpQuery throwing errors that it cant read the HTML file/text.
     * This function simply removed that bit from the string and has so far never
     * caused any problem. So you should be able to use it on any HTML string
     * regardless.
     */
```