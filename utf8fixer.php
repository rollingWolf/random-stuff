<?php
/**
* I have a habit of scraping websites. Sometimes the encoding messes up
* depending on how you scrape so this is an ugly hack I use for making
* sure that its properly UTF8 encoded.
*
* remove_utf8_bom($text) should be self explenatory
*/
function is_utf8($text)
{
    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
    [\x09\x0A\x0D\x20-\x7E]            # ASCII
    | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
    |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
    | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
    |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
    |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
    | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
    |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$%xs', $text);
}
function remove_utf8_bom($text)
{
    $bom = pack('H*','EFBBBF');
    $text = preg_replace("/^($bom)+/", '', $text);

    return $text;
}
function fix_utf8($text)
{
    if (is_utf8($text)) {
        while(true) {
            if (!is_utf8($text))
                break;
            if(DEBUG) echo $text.PHP_EOL;
            $text = utf8_decode($text);
        }
        if(DEBUG) echo '---- DONE ';
        $text = utf8_encode($text);
    } else {
        while(true) {
            if (is_utf8($text))
                break;
            if(DEBUG) echo $text.PHP_EOL;
            $text = utf8_encode($text);
        }
        if(DEBUG) echo '---- DONE ';
    }
    return $text; //utf8_encode($text);
}
