<?php
/*
 * jscdn basic class linking to cdnjs.com. Not endorsed in any way.
 *
 * Copyright (c) 2014 Markus Noren
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class jsCDN to link to JS/CSS files on a known CDN
 *
 * @category cdn, loadbalancing
 * @package jsCDN
 * @author Markus Noren <jastormereel@gmail.com>
 * @copyright Markus Noren
 * @version 0.4
 * @link https://bitbucket.org/jastor/random-stuff/
 */
class jscdn
{
    public $cacheDir = '/cache';
    public $cacheFile = 'packages.json';
    public $cacheCleanFile = 'packages.clean.json';
    public $cacheLifetime = '+10weeks';
    public $cacheLifetimeClean = '+1day';
    public $packagesJSON = 'http://cdnjs.com/packages.json';
    public $CDNurl = '//cdnjs.cloudflare.com/ajax/libs/%NAME%/%VERSION%/%FILENAME%';
    /**
     * __construct
     * Parse any settings passed, download json with information about packages
     *
     *
     * @return none
     */
    public function __construct($settings = array())
    {
        $this->parseSettings($settings);
        if (!is_dir($cacheDir)) {
            mkdir($cacheDir);
        }
        $this->cacheDir = dirname(__FILE__).$this->cacheDir;
        if(!is_file($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile)
            || strtotime($this->cacheLifetime, @filemtime($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile)) < time()
            ) {
            $_JSON = file_get_contents($this->packagesJSON);
            file_put_contents($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile, $_JSON);
        } else {

        }
    }
    /**
     * parseSettings
     * Parse settings passed to construct and set them
     *
     * @param $settings
     */
    public function parseSettings($settings)
    {
        if (count($settings)) {
            foreach ($settings as $settingKey => $settingVal) {
                if (isset($this->$settingKey)) {
                    $this->$settingKey = $settingVal;
                }
            }
        }
    }
    /**
     * requireFiles
     * Parse array with js and or cdn files that we want to use
     *
     * @param $files
     */
    public function requireFiles($files = array(), $refresh = false)
    {
        //!is_file($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile)

        if( true === $refresh
            || strtotime($this->cacheLifetimeClean, @filemtime($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheCleanFile)) < time()
            ) {
            $_JSON = json_decode(file_get_contents($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile), true);
        } else {
            $_JSON = json_decode(file_get_contents($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheCleanFile), true);
            $_skipClean = true;
        }
        foreach ($_JSON['packages'] as $_index => $_package) {
            $_name = $_package['name'];
            $_version = $_package['version'];
            if (array_key_exists($_name, $files)) {
                if ($_package['version'] === $files[$_name]['version']) {
                    $this->found[] = $_index;
                    $this->filename[$_name] = array(
                        'version' => $_package['version'],
                        'filename' => $_package['filename']
                        );
                    if (!isset($files[$_name]['includeMatch'])) {
                        continue;
                    }
                    $this->match[$_name] = $files[$_name]['includeMatch'];
                    $this->assets[$_name] = $_package['assets'][0]['files'];
                } else {
                    $_found = false;
                    foreach ($_package['assets'] as $_asset) {
                        if ($_asset['version'] === $files[$_name]['version']) {
                            $this->filename[$_name] = array(
                                'version' => $_asset['version'],
                                'filename' => $_package['filename']
                                );
                            $this->assets[$_name] = $_asset['files'];
                            $_found = true;
                            break;
                        }
                    }
                    if (!$_found) {
                        $this->filename[$_name] = array(
                            'version' => $_package['version'],
                            'filename' => $_package['filename']
                            );
                        $this->assets[$_name] = $_package['assets'][0]['files'];
                    }
                }
                $this->found[] = $_index;
                continue;
            } else {
                continue;
            }
        }
        ksort($this->filename);
        if (array_key_exists('local', $files)) {
            foreach ($files['local'] as $localfile) {
                $this->filename['localFiles'][] = $localfile;
            }
        }
        if (count($this->found) && !$_skipClean) {
            $this->saveCachedFile();
        }
    }
    /**
     * saveCachedFile
     * Save clean json with only information about packages we actually use
     */
    public function saveCachedFile()
    {
        if (count($this->found)) {
            $_JSON = json_decode(file_get_contents($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheFile), true);
            $_OUTPUT = array();
            foreach ($this->found as $_index) {
                $_OUTPUT['packages'][] = $_JSON['packages'][$_index];
            }
            file_put_contents($this->cacheDir.DIRECTORY_SEPARATOR.$this->cacheCleanFile, json_encode($_OUTPUT));
        }
    }
    /**
     * parseIncludes
     * Get actual links to the packages and includes
     */
    public function parseIncludes()
    {
        if (count($this->filename['localFiles'])) {
            foreach ($this->filename['localFiles'] as $_file) {
                $_locals[] = $_file;
            }
            unset($this->filename['localFiles']);
        }
        foreach ($this->filename as $_name => $_file) {
            $cdnfile = $this->CDNurl;
            $cdnfile = str_replace('%NAME%', $_name, $cdnfile);
            $cdnfile = str_replace('%VERSION%', $_file['version'], $cdnfile);
            $cdnfile = str_replace('%FILENAME%', $_file['filename'], $cdnfile);
            $this->cdnfile[] = $cdnfile;
            if (isset($this->match[$_name])) {
                $_match = '#('.strtolower(implode('|', $this->match[$_name])).')#i';
                foreach ($this->assets[$_name] as $_asset) {
                    if (preg_match($_match, $_asset)) {
                        $cdnfile = $this->CDNurl;
                        $cdnfile = str_replace('%NAME%', $_name, $cdnfile);
                        $cdnfile = str_replace('%VERSION%', $_file['version'], $cdnfile);
                        $cdnfile = str_replace('%FILENAME%', $_asset, $cdnfile);
                        $this->cdnfile[] = $cdnfile;
                        continue;
                    }
                }
            }
        }
        $this->cdnfile = array_merge($this->cdnfile, $_locals);
    }
    /**
     * getIncludes
     * Return <script> and <link> tags to files
     *
     * @param $extension Extension of files to load
     *
     * @return Array with html code of includes
     */
    public function getIncludes($extension = 'js')
    {
        $this->parseIncludes();
        foreach ($this->cdnfile as $cdnfile) {
            preg_match('#(P?<ext>'.$extension.')$#i', $cdnfile, $_ext);
            switch ($_ext['ext']) {
                case 'js':
                    $this->printIncludes[] = '<script type="text/javascript" src="'.$cdnfile.'"></script>';
                    break;
                case 'css':
                    $this->printIncludes[] = '<link rel="stylesheet" type="text/css" href="'.$cdnfile.'">';
                    break;
            }

        }

        return $this->printIncludes;
    }
}
/*
$settings = array(
    'cacheDir' => 'cache/',
    );
$jsCDN = new jsCDN($settings);
$files = array(
    'jquery' => array('version' => '2.1.0'),
    'jqueryui' => array(
        'version' => '1.10.4',
        'includeMatch' => array(    //load css matching this expression
            'jquery-ui.min.css$'
        ),
    ),
    'local' => array(   //load local files not from the CDN
        'js/main.js',
        'css/style.css',
        )
    );
$jsCDN->requireFiles($files);
$jsCDN->getIncludes('js|css');
foreach ($jsCDN->printIncludes as $include) {
    echo $include;
}
*/
