#EasyCacheHeaders
Simple function to send and check cache headers. This is not used for caching filesit does not actually cache the file on server, this script only provides easy access to compare contents to browser cache.

In all examples this is written in the file index.php before any output and referrs to itself.

##EXAMPLE 1:
Standard usage. Send cache headers and exit if matched. In this case only Last-Modified is matched against.
```php
    $thisFile = __FILE__;
    $context = filemtime($thisFile);
    if(tryCache($context)) exit; // matched and 304 Not Mdofied header sent
```

##EXAMPLE 2:
Generate checksum for use with ETag header
```php
    $thisFile = __FILE__;
    /**
     * Feel free to write your own checksum generating script, this is just
     * an example
     */
    $filestat = stat($thisFile);
    $checksum = sprintf(
        '%x-%x-%s',
        $filestat['ino'],
        $filestat['size'],
        base_convert(str_pad($filestat['mtime'], 16, '0'), 10, 16)
    );
    $context = array(
        'mtime' => filemtime($thisFile),
        'checksum' => $checksum
        );
    if(tryCache($context)) exit;
```

##EXAMPLE 3:
Provide custom expire timestamp.
```php
    $thisFile = __FILE__;
    $checksum = '<checksum>';
    $context = array(
        'mtime' => filemtime($thisFile),
        'checksum' => $checksum,
        'expires' => strtotime('+12 days')
        );
    if(tryCache($context)) exit;
```

##Default settings
You can change a bunch of settings by changing the defines in the beginning of the file. I suggest you only change AUTOHALT, MAXAGE, EXPIRE, and the USE_* settings. The default settings should work out of the box even though only tested with Chrome on MAC OSX.