<?php
/* DO NOT CHANGE UNLESS YOU ABSOLUTELY KNOW WHAT YOURE DOING */
/* DO NOT CHANGE UNLESS YOU ABSOLUTELY KNOW WHAT YOURE DOING */
define('CACHE_DATE', 'D, d M Y H:i:s \G\M\T');
/* /DO NOT CHANGE UNLESS YOU ABSOLUTELY KNOW WHAT YOURE DOING */
/* /DO NOT CHANGE UNLESS YOU ABSOLUTELY KNOW WHAT YOURE DOING */


/* DURATIONS, No need to change */
define('DUR_MINUTE', 60);
define('DUR_HOUR', 60*DUR_MINUTE);
define('DUR_DAY', 24*DUR_HOUR);
define('DUR_WEEK', 7*DUR_DAY);
define('DUR_30MONTH', 30*DUR_DAY);
define('DUR_31MONTH', 31*DUR_DAY);
define('DUR_YEAR', 365*DUR_DAY);


/* DEFAULT SETTINGS, CHANGE AS YOU PLEASE */
define('CACHE_AUTOHALT', true);     // automatically halt script when matched
define('CACHE_MAXAGE', 2*DUR_HOUR); // max age
define('CACHE_EXPIRE', DUR_WEEK);   // expire in how long
define('CACHE_USE_CACHE_CONTROL_MAX_AGE', true);
define('CACHE_USE_CACHE_CONTROL_MUST_REVALIDATE', true);
define('CACHE_USE_ETAG', true);
define('CACHE_USE_EXPIRES', true);
define('CACHE_USE_LAST_MODIFIED', true);

/**
 * $context may be either:
 * An integer with an timestamp
 *
 * OR
 *  REQUIRED: 'mtime'       Timestamp of file modification
 *  OPTIONAL: 'checksum'    ETag checksum
 *  OPTIONAL: 'expires'     Non default expiration
 *
 * @return (bool) true if the page is cached by browser, false if not
 * You would use the return to halt the script if file is cached by browser.
 */
function tryCache($context)
{
    $headers = apache_request_headers();
    $etagMatch = false;
    $modMatch = false;

    if (is_int($context)) {
        $lastModified = $context;
    } elseif (is_array($context) && isset($context['mtime'])) {
        $lastModified = $context['mtime'];
        if (CACHE_USE_ETAG) {
            if (isset($context['checksum'])) {
                $etagFile = $context['checksum'];
                // match etag headers (bool) true / false
                $etagMatch = (isset($headers['If-None-Match']) and trim($headers['If-None-Match']) == '"'.$etagFile.'"');

                header('ETag: "'.$etagFile.'"');
            } else {
                echo '$context is not of required type';
                return false;
            }
        }
    } else {
        echo '$context is not of required type';
        return false;
    }

    header(
        'Cache-Control: '.
        'public'.
        ((CACHE_USE_CACHE_CONTROL_MAX_AGE) ? ', max-age='.CACHE_MAXAGE : '').
        ((CACHE_USE_CACHE_CONTROL_MUST_REVALIDATE) ? ', must-revalidate' : '')
    );

    if (CACHE_USE_EXPIRES) {
        $expires = (isset($context['expires'])) ? $context['expires'] : (time() + CACHE_EXPIRE);
        header('Expires: '.gmdate(CACHE_DATE, $expires));
    }
    if (CACHE_USE_LAST_MODIFIED) {
        // match modified headers (bool) true / false
        $modMatch = (isset($headers['If-Modified-Since']) and strtotime($headers['If-Modified-Since']) == $lastModified);
        header('Last-Modified: '.gmdate(CACHE_DATE, $lastModified));
    }
    header("Pragma: cache");

    if ($modMatch || $etagMatch) {
        header('HTTP/1.1 304 Not Modified');
        if (CACHE_AUTOHALT) {
            exit;
        }
        return true;
    }

    return false;
}
