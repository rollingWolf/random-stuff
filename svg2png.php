<?php
/*
 * svg2png basic class using imagick to convert svg to
 * png setting simple parameters like colors and dimensions.
 *
 * Copyright (c) 2014 Markus Noren
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * svg2png main class
 *
 * @vesion  0.1.1
 * @package svg2png
 * @author  Markus Noren <jastormereel@gmail.com>
 */
class svg2png
{
    /**
     * __construct
     * Initiate Imagick, fail if class doesnt exist
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function __construct()
    {
        if (!extension_loaded('imagick')) {
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                dl('php_imagick.dll');
            } else {
                dl('imagick.so');
            }
            if (!extension_loaded('imagick')) {
                echo 'Class: "Imagick" not found. Required!'.PHP_EOL;
                exit;
            }
        }
        $this->im = new Imagick();
    }
    /**
     * setBG
     * Insert description here
     *
     * @param $bg ImagickPixel to set as background color, defaults to transparent
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function setBG($bg = '')
    {
        if ($bg) {
            $this->im->setBackgroundColor($bg);
        } else {
            $this->im->setBackgroundColor(new ImagickPixel('transparent'));
        }
    }
    /**
     * setFG
     * Change one color
     *
     * @param $from What color to change
     * @param $to   Color to change into,
     *        optional: to to from and from to black
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function setFG($from, $to = '')
    {
        if (!$to) {
            $to = $from;
            $from = Imagick::CHANNEL_BLACK;
        }
        $this->im->floodFillPaintImage($to, 0, '', 0, 0, $from);
    }
    /**
     * setFormat
     * What image format to save output as
     *
     * @param $format
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function setFormat($format = 'png')
    {
        $this->format = $format;
        $this->im->setImageFormat($format);
    }
    /**
     * setSize
     * Output file dimensions
     *
     * @param $height Output image height in pixels
     * @param $width  Optional: defaults to height
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function setSize(int $height, $width = 0)
    {
        if (!$width) {
            $width = $height;
        }
        $this->im->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
    }
    /**
     * load
     * Load file to convert from
     *
     * @param $inFile Filename
     *
     * @return
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function load($inFile)
    {
        if (!is_file($inFile) || !is_readable($inFile)) {
            echo 'Cannot read "'.$inFile.'" for reading'.PHP_EOL;
            exit;
        }
        $this->im->readImage($inFile);
    }
    /**
     * write
     * Write output image to file
     *
     * @param $outFile Filename to write to
     *
     * @access
     * @static
     * @see
     * @since
     */
    public function write($outFile)
    {
        if (is_file($outFile) && !is_writeable($outFile)) {
            echo 'Cannot open "'.$outFile.'" for writing'.PHP_EOL;
            exit;
        }
        if (!isset($this->format)) {
            preg_match('~\.(?P<ext>\w+)$~Ui', $outFile, $ext);
            $this->setFormat($ext['ext']);
        }
        $fp = fopen($outFile, 'w');
        fwrite($fp, $this->im);
        fclose($fp);
    }
}

$svg2png = new svg2png();
$svg2png->setBG(); // set background to transparent
$svg2png->load('icons/script.svg');
$svg2png->setFG('orange');  // switch black foreground to orange
$svg2png->setSize(256); // output square image 256x256 pixels
$svg2png->write('script.png');
