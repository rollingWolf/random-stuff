#password
PHP CLI app that utilizes phpSec to generate a secure mnemonic password. For random useless extra information Ive also implemented parts of [CubicleSoft](http://barebonescms.com) [Single Sign On](http://barebonescms.com/documentation/sso/) to get a float measure of how secure the password is compared to wordlist, randomness and whatnot.

##Installation
Package requires [phpSec](http://phpseclib.com) obtainable through [Composer](https://getcomposer.org)
`composer require phpsec/phpsec:dev-master`

##Usage
You must set +x on password to be able to be call it directly but you can otherwise use
`php password --new`

`./password --new[=keysize]`
Generate new password.
Keysize is optional, defaults to 64. Minimum recommended value is 24 lower than that and the randomness of the words is not so random.

Secret and passphrase are interchangable, if you know one you know the other.

`./password --secret`
Convert hexadecimal secret into a password.

`./password --pass`
Convert passphrase into hexadecimal secret

You can ofcourse pipe textfiles and similiar to pass
`./password --pass < myfilewithpassphrase.txt`

Any words not avaible in phpsec mnemonic wordlist will be filtered out. Be sure to compare the PASSPHRASE: printed out to the words you use.

##Sample output
    ./password --new
    KEYSIZE: 64
    SECRET: 60944A239C8A9488
    PASSPHRASE: silently tie dim slam walk trace
    STRENGTH: 41.00390625

    ./password --new=128
    KEYSIZE: 128
    SECRET: FB6849631A022D82FD7464BE2155D29C
    PASSPHRASE: grow purpose teacher fire bright quickly perhaps strike dinner white crave guard
    STRENGTH: 65.343552589417

    ./password --secret
    SECRET: FFF90ABCDEF990A       <- read input with stream_get_line(stdin)
    SECRET: FFF90ABCDEF990A
    PASSPHRASE: rainbow yourself since bright root breast
    STRENGTH: 48.1484375

    ./password --pass < textwithsomewords.txt
    PASSPHRASE: SECRET: EBCFE5A973AA6A50EB7CF025
    PASSPHRASE: hello name know god something among other mountain snow fire
    STRENGTH: 54.636367797852

#Disclaimer
I made this because I want unique passwords for every login and Im too lazy to come up with something by myself.