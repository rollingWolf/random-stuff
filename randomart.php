<?php
/*
 * Basic class generating a randomart image from fingerprint.
 *
 * Copyright (c) 2014 Markus Noren
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Basic class generating a randomart image from fingerprint.
 *
 * @category ssh
 * @package RandomArt
 * @author Markus Noren <jastormereel@gmail.com>
 * @copyright Markus Noren
 * @version 1.0
 * @link https://bitbucket.org/jastor/random-stuff/
 */

class RandomArt
{
    public static function generate($fingerprint)
    {
        $board = 'aTTTTTTTTTTTTTTTbLMMMMMMMMMMMMMMMRLMMMMMMMMMMMMMMMRLMMMMMMMMMMMMMMMRLMMMMMMMSMMMMMMMRLMMMMMMMMMMMMMMMRLMMMMMMMMMMMMMMMRLMMMMMMMMMMMMMMMRcBBBBBBBBBBBBBBBd';
        $freq = array(
                '',  '.', 'o', '+', '=', '*', 'B', 'O',
                'X', '@', '%', '&', '#', '/', '^'
            );
        $validMoves = array(
            'S' => array('00' => -18, '01' => -16, '10' => 16, '11' => 18),
            'M' => array('00' => -18, '01' => -16, '10' => 16, '11' => 18),
            'a' => array('00' =>   0, '01' =>   1, '10' => 17, '11' => 18),
            'b' => array('00' =>  -1, '01' =>   0, '10' => 16, '11' => 17),
            'c' => array('00' => -17, '01' => -16, '10' =>  0, '11' =>  1),
            'd' => array('00' => -18, '01' => -17, '10' => -1, '11' =>  0),
            'T' => array('00' =>  -1, '01' =>   1, '10' => 16, '11' => 18),
            'B' => array('00' => -18, '01' => -16, '10' => -1, '11' =>  1),
            'R' => array('00' => -18, '01' => -17, '10' => 16, '11' => 17),
            'L' => array('00' => -17, '01' => -16, '10' => 17, '11' => 18),
        );
        $startPosition = $currentPosition = strpos($board, 'S');
        $hexes = preg_split('~:~', $fingerprint);
        $points = array();
        foreach ($hexes as $hexMove) {
            $binMove = str_pad(decbin(hexdec($hexMove)), 8, '0', STR_PAD_LEFT);
            preg_match_all('~([10]{2})~', $binMove, $match);
            foreach (array_reverse($match[1]) as $doMove) {
                $currentValue = substr($board, $currentPosition, 1);
                $currentPosition += $validMoves[$currentValue][$doMove];
                @$points[$currentPosition] ++;
            }
        }
        $lines = array_filter(preg_split('~~', $board));
        foreach ($points as $pos => $val) {
            $lastPosition = ($pos+1);
            $lines[$lastPosition] = $freq[$val];
        }
        $lines[$lastPosition] = 'E';
        $lines[$startPosition+1] = 'S';
        $output = implode($lines);
        preg_match_all('~(.{17})~', $output, $match);
        $randomart = '+--[ RSA 2048]----+'.PHP_EOL;
        foreach ($match[1] as $text) {
            $text = strtr($text, 'abcdTBMLR', '         ');
            $randomart .= '|'.$text.'|'.PHP_EOL;
        }
        $randomart .= '+-----------------+';
        return $randomart;
    }
}
