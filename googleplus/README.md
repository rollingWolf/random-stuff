#[jastor / Random Stuff / googleplus](https://bitbucket.org/jastor/random-stuff/src/725a4758d69174c8304739495a30921b3ff1bfa4/googleplus/)
A phpclass to easily handle Google+ javascript API for sharing, following, +1, badges for pages, groups and profiles. Also have limited support for opengraph.

For easy access, when including googleplus.php GOOGLEPLUS_URL_TO_SELF will be defined to the url to the page according to whats avaible in $_SERVER._

##Basic Usage
Begin with:

    require_once 'googleplus.php';
    $gp = new jastor\googleplus;

End with:

    echo $gp->JS(true | false);
    /**
     Echo <script> tag for including googleplus. Pass true as parameter for asynchrous load.
     */

In between:
FOr more details on the fields please visit [developers.google.com](http://developers.google.com/)

    echo $gp->set("ID","https://plus.google.com/+JastorMereel")->Author();
    // Echo <link rel="author"> and my Google+ profile
    echo $gp->set("ID","https://plus.google.com/115830407295563307897")->Publisher();
    // Echo <link rel="publisher"> and Zend's Google+ page

    echo $gp->PlusOne();
    /**
    Echo <div> required for +1
    Optional fields (use $gp->set(field, value); all fieldnames are uppercase)
        SIZE                standard, small, medium, tall
        ANNOTATION          bubble, none, online
        ALIGN               left, right
        RECOMMENDATIONS     bool
        COUNT               bool
    ***/
    echo $gp->Share();
    /**
    Echo <div> required to Share
    Optional fields
        ANNOTATION          bubble, vertical-bubble, none
        HEIGHT              20, 15, 24, 60
        ALIGN               left, right
    ***/
    echo $gp->Follow();
    /**
    Echo <div> required to follow
    Required fields
        ID                  https://plus.google.com/<+identifier | 21 digits>
    Optional fields
        ANNOTATION          bubble, vertical-bubble, none
        HEIGHT              20, 15, 24
        RELATIONSHIP        '', author, publisher
    ***/
    echo $gp->Comments();
    /**
    Echo <div> required for comments
    Required fields
        HREF                   <url>
    ***/
##Badges
Please look at googleplus.php for avaible and required fields.

    echo $gp->Profile();
    /**
    Echo <div> required for Profile badge
    ***/
    echo $gp->Page();
    /**
    Echo <div> required for Page badge
    ***/
    echo $gp->Community();
    /**
    Echo <div> required for Community badge. Does NOT use "ID" but "COMMINITYID"
    Required fields
        COMMUNITYID     <url> to community
    ***/
    echo $gp->Followers();
    /**
    Echo <div> required for Followers badge
    ***/
##OpenGraph

    echo $gp->OpenGraph();
    /**
    Print opengraph <meta>
    Required fields
        OG_TITLE
        OG_TYPE     defaults to "website"
        OG_IMAGE
        OG_URL      defaults to GOOGLEPLUS_URL_TO_SELF
    Optional fields
        OG_LOCALE
        OG_SITE_NAME
        OG_DESCRIPTION
        OG_TAGS
        OG_CATEGORY
        OG_SITE_OWNER
        OG_PUBLISH_DATE
    ***/
version: 1.1