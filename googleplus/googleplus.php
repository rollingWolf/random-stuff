<?php
namespace jastor;

/*
 * Class for quick and easy googleplus javascript apiaccess
 *
 * Copyright (c) 2014 Markus Noren
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

define('GOOGLEPLUS_ASYNCHRONOUS', true);
define('GOOGLEPLUS_DOMAIN', 'http'.((isset($_SERVER['HTTPS']) || $_SERVER['SERVER_PORT'] === 443) ? 's':'').'://'.$_SERVER['SERVER_NAME']);
define('GOOGLEPLUS_URL_TO_SELF', GOOGLEPLUS_DOMAIN.$_SERVER['REQUEST_URI']);

/**
 * googleplus
 * Class to easy access google+ share javascript api
 *
 * @category social, sharing
 * @package jastor\googleplus
 * @author Markus Noren <jastormereel@gmail.com>
 * @copyright Markus Noren
 * @version 1.1
 * @link https://bitbucket.org/jastor/random-stuff/
 */
class googleplus
{
    /**
     * Types is a list of regular expressions that the KEY must match
     */
    private $TYPES = array(
        'ID' => array(
            'https://plus.google.com/ and 21 digits'=>'~^https:\/\/plus\.google\.com\/[0-9]{21}~',
            'https://plus.google.com/ and Starting with +'=>'~^https:\/\/plus\.google\.com\/\+~'
            ),
        'COMMUNITYID' => array(
            'https://plus.google.com/communities/ and 21 digits' => '~https:\/\/plus\.google\.com\/communities\/[0-9]{21}~',
            'https://plus.google.com/communities/ and Starting with +' => '~https:\/\/plus\.google\.com\/communities\/\+~'
            ),
        'HREF' => array(
            'Staring with http(s)://' => '~^https?:\/\/~',
            )
        );
    /**
     * Default settings and valid values
     */
    private $DEFAULT_SETTINGS = array(
        'plusone' => array(
            'SIZE' => array('standard','small','medium','tall'),
            'ANNOTATION' => array('bubble','none','inline'),
            'ALIGN' => array('left','right'),
            'RECOMMENDATIONS' => array('true','false'),
            'COUNT' => array('true','false'),
            ),
        'share' => array(
            'ANNOTATION' => array('bubble','vertical-bubble','none'),
            'HEIGHT' => array(20,15,24,60),
            'ALIGN' => array('left','right'),
            ),
        'follow' => array(
            'ANNOTATION' => array('bubble','vertical-bubble','none'),
            'HEIGHT' => array(20,15,24),
            'RELATIONSHIP' => array('','author','publisher'),
            'required' => array('ID'),
            ),
        'comments' => array(
            'HEIGHT' => array(50),
            'WIDTH' => array(450),
            'required' => array('HREF'),
            ),
        'publisher' => array(
            'required' => array('ID'),
            ),
        'author' => array(
            'required' => array('ID'),
            ),
        'profile' => array(
            'WIDTH' => array(180,273,300,450),
            'THEME' => array('lights','dark'),
            'LAYOUT' => array('portrait','landscape'),
            'SHOWCOVER' => array('true','false'),
            'SHOWTAGLINE' => array('true','false'),
            'AUTHOR' => array('true','false'),
            'required' => array('ID'),
            ),
        'page' => array(
            'WIDTH' => array(180,273,300,450),
            'THEME' => array('lights','dark'),
            'LAYOUT' => array('portrait','landscape'),
            'SHOWCOVER' => array('true','false'),
            'SHOWTAGLINE' => array('true','false'),
            'PUBLISHER' => array('true','false'),
            'required' => array('ID'),
            ),
        'community' => array(
            'WIDTH' => array(180,273,300,450),
            'THEME' => array('lights','dark'),
            'LAYOUT' => array('portrait','landscape'),
            'SHOWPHOTO' => array('true','false'),
            'SHOWOWNERS' => array('true','false'),
            'SHOWTAGLINE' => array('true','false'),
            'required' => array('COMMUNITYID'),
            ),
        'followers' => array(
            'required' => array('ID','ISAUTHOR'),
            ),
        'opengraph' => array(
            'required' => array('OG_TITLE','OG_TYPE','OG_IMAGE','OG_URL'),
            ),
        );
    public $ogTags = array(
        'title' => '',
        'type' => 'website',
        'image' => '',
        'url' => GOOGLEPLUS_URL_TO_SELF,
        'locale' => '',
        'site_name' => '',
        'description' => '',
        'tags' => '',
        'category' => '',
        'site_owner' => '',
        'publish_date' => '',
        );
    /**
     * Pre-set values. Change with function: set
     */
    public $SETTINGS = array(
        'LANG' => 'en-US', // https://developers.google.com/+/web/api/supported-languages
        'PARSETAGS' => 'onload', // either onload or explicit
        );
    /**
     * set
     * Set setting
     *
     * @param $key
     * @param $value
     *
     * @return $this
     *
     */
    public function set($key, $value)
    {
        $this->SETTINGS[$key] = $value;

        return $this;
    }

    /**
     * checkSettings
     * Check that required settings are set, set default settings
     *
     * @param $type Type of settings, plusone, share or follow
     *
     * @return bool, true on success, false otherwise
     *
     * @access private
     */
    private function checkSettings($type)
    {
        if (count($this->DEFAULT_SETTINGS[$type]['required'])) {
            foreach ($this->DEFAULT_SETTINGS[$type]['required'] as $required) {
                $matched = false;
                if (!isset($this->SETTINGS[$required])) {
                    echo '<!--'.PHP_EOL;
                    echo 'Required field "'.$required.'" missing!'.PHP_EOL;
                    echo '-->';

                    return false;
                }
                if (isset($this->TYPES[$required])) {
                    foreach ($this->TYPES[$required] as $description => $match) {
                        if (preg_match($match,$this->SETTINGS[$required])) {
                            $matched = true;
                            continue;
                        }
                    }
                } else {
                    if ($this->SETTINGS[$required] !== '') {
                        continue;
                    }
                }
                if ($matched) {
                    continue;
                } else {
                    echo '<!--'.PHP_EOL;
                    echo 'Required field "'.$required.'" doesnt match any required type!'.PHP_EOL;
                    echo 'Accepted value is (any): '.implode(', ',array_keys($this->TYPES[$required])).PHP_EOL;
                    echo '-->';

                    return false;
                }
            }
        }
        foreach ($this->SETTINGS as $sk => $sv) {
            if (array_key_exists($sk,$this->TYPES) && !is_array($this->TYPES[$sk])) {
                if (!preg_match($this->TYPES[$sk],$sv)) {
                    echo '<!--'.PHP_EOL;
                    echo 'Optional field "'.$sk.'" doesnt match any required type!'.PHP_EOL;
                    echo $sk.'='.$sv.PHP_EOL;
                    echo 'Accepted value matches: '.$this->TYPES[$sk].PHP_EOL;
                    echo '-->';

                    return false;
                }
            }
        }
        foreach ($this->DEFAULT_SETTINGS[$type] as $sk => $sv) {
            if (isset($this->SETTINGS[$sk])) {
                if (in_array($this->SETTINGS[$sk],$sv)) {
                    continue;
                }
            }
            $this->SETTINGS[$sk] = $sv[0];
        }

        return true;
    }
    /**
     * PlusOne
     * htmlcode for Google+ +1
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function PlusOne()
    {
        if ($this->checkSettings('plusone')) {
            $code[] = '<div class="g-plusone';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = (($this->SETTINGS['HREF']) ? 'data-href="'.$this->SETTINGS['HREF'].'"':'');
            $code[] = 'data-size="'.$this->SETTINGS['SIZE'].'"';
            $code[] = 'data-annotation="'.$this->SETTINGS['ANNOTATION'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = 'data-align="'.$this->SETTINGS['ALIGN'].'"';
            $code[] = (($this->SETTINGS['EXPANDTO'] > 0) ? 'expandTo="'.$this->SETTINGS['EXPANDTO'].'"' : '');
            $code[] = (($this->SETTINGS['CALLBACK'] > 0) ? 'data-callback="'.$this->SETTINGS['CALLBACK'].'"' : '');
            $code[] = (($this->SETTINGS['ONSTARTINTERACTION']) ? 'data-onstartinteraction="'.$this->SETTINGS['ONSTARTINTERACTION'].'"':'');
            $code[] = (($this->SETTINGS['ONENDINTERACTION']) ? 'data-onendinteraction="'.$this->SETTINGS['ONENDINTERACTION'].'"':'');
            $code[] = 'data-recommendations="'.$this->SETTINGS['RECOMMENDATIONS'].'"';
            $code[] = 'data-count="'.$this->SETTINGS['COUNT'].'"';
            $code[] = '></div>';
            echo $this->cleanOutput($code);
        }
    }
    /**
     * Share
     * htmlcode for Google+ share
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Share()
    {
        if ($this->checkSettings('share')) {
            $code[] = '<div class="g-plus';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-action="share"';
            $code[] = (($this->SETTINGS['HREF']) ? 'data-href="'.$this->SETTINGS['HREF'].'"':'');
            $code[] = 'data-annotation="'.$this->SETTINGS['ANNOTATION'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = 'data-height="'.$this->SETTINGS['HEIGHT'].'"';
            $code[] = 'data-align="'.$this->SETTINGS['ALIGN'].'"';
            $code[] = (($this->SETTINGS['EXPANDTO'] > 0) ? 'data-expandTo="'.$this->SETTINGS['EXPANDTO'].'"' : '');
            $code[] = (($this->SETTINGS['ONSTARTINTERACTION']) ? 'data-onstartinteraction="'.$this->SETTINGS['ONSTARTINTERACTION'].'"':'');
            $code[] = (($this->SETTINGS['ONENDINTERACTION']) ? 'data-onendinteraction="'.$this->SETTINGS['ONENDINTERACTION'].'"':'');
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Follow
     * htmlcode for Google+ follow
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Follow()
    {
        if ($this->checkSettings('follow')) {
            $code[] = '<div class="g-follow';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-href="'.$this->SETTINGS['ID'].'"';
            $code[] = 'data-annotation="'.$this->SETTINGS['ANNOTATION'].'"';
            $code[] = 'data-height="'.$this->SETTINGS['HEIGHT'].'"';
            $code[] = (($this->SETTINGS['RELATIONSHIP']) ? 'data-rel="'.$this->SETTINGS['RELATIONSHIP'].'"':'');
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Comments
     * htmlcode for Google+ comments
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Comments()
    {
        if ($this->checkSettings('comments')) {
            $code[] = '<div class="g-comments';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = (($this->SETTINGS['HREF']) ? 'data-href="'.$this->SETTINGS['HREF'].'"':'');
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = (($this->SETTINGS['HEIGHT'] > 0) ? 'data-height="'.$this->SETTINGS['HEIGHT'].'"' : '');
            $code[] = 'data-first_party_property="'.$this->SETTINGS['SOURCE'].'"';
            $code[] = 'data-view_type="FILTERED_POSTMOD"';
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Profile
     * htmlcode for Google+ Profile
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Profile()
    {
        if ($this->checkSettings('profile')) {
            $code[] = '<div class="g-person';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-href="'.$this->SETTINGS['ID'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = 'data-layout="'.$this->SETTINGS['LAYOUT'].'"';
            $code[] = 'data-theme="'.$this->SETTINGS['THEME'].'"';
            $code[] = 'data-showcoverphoto="'.$this->SETTINGS['SHOWCOVER'].'"';
            $code[] = 'data-showtagline="'.$this->SETTINGS['SHOWTAGLINE'].'"';
            $code[] = (($this->SETTINGS['AUTHOR']) ? 'data-rel="author"':'');
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Followers
     * htmlcode for Google+ Followers
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Followers()
    {
        if ($this->checkSettings('followers')) {
            $code[] = '<div class="g-plus';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-action="followers"';
            $code[] = 'data-href="'.$this->SETTINGS['ID'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = (($this->SETTINGS['HEIGHT'] > 0) ? 'data-height="'.$this->SETTINGS['HEIGHT'].'"' : '');
            $code[] = (($this->SETTINGS['ISAUTHOR']) ? 'data-rel="author"':'data-rel="publisher"');
            $code[] = 'data-source="blogger:blog:followers"';
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Page
     * htmlcode for Google+ Page
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Page()
    {
        if ($this->checkSettings('page')) {
            $code[] = '<div class="g-page';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-href="'.$this->SETTINGS['ID'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = 'data-layout="'.$this->SETTINGS['LAYOUT'].'"';
            $code[] = 'data-theme="'.$this->SETTINGS['THEME'].'"';
            $code[] = 'data-showcoverphoto="'.$this->SETTINGS['SHOWCOVER'].'"';
            $code[] = 'data-showtagline="'.$this->SETTINGS['SHOWTAGLINE'].'"';
            $code[] = (($this->SETTINGS['PUBLISHER']) ? 'data-rel="publisher"':'');
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * Community
     * htmlcode for Google+ Community
     *
     *
     * @return htmlcode
     *
     * @access public
     */
    public function Community()
    {
        if ($this->checkSettings('community')) {
            $code[] = '<div class="g-community';
            $code[] = (($this->SETTINGS['CLASS']) ? $this->SETTINGS['CLASS'] : '').'"';
            $code[] = 'data-href="'.$this->SETTINGS['COMMUNITYID'].'"';
            $code[] = (($this->SETTINGS['WIDTH'] > 0) ? 'data-width="'.$this->SETTINGS['WIDTH'].'"' : '');
            $code[] = 'data-layout="'.$this->SETTINGS['LAYOUT'].'"';
            $code[] = 'data-theme="'.$this->SETTINGS['THEME'].'"';
            $code[] = 'data-showphoto="'.$this->SETTINGS['SHOWPHOTO'].'"';
            $code[] = 'data-showowners="'.$this->SETTINGS['SHOWOWNERS'].'"';
            $code[] = 'data-showtagline="'.$this->SETTINGS['SHOWTAGLINE'].'"';
            $code[] = '></div>';

            return $this->cleanOutput($code);
        }
    }
    /**
     * OpenGraph
     * <meta> tags for OpenGraph
     *
     *
     * @return <meta> tags
     *
     * @access public
     */

    public function OpenGraph()
    {
        foreach ($this->ogTags as $ogname => $ogvalue) {
            if (!isset($this->SETTINGS['OG_'.strtoupper($ogname)]) && $ogvalue !== "") {
                $this->SETTINGS['OG_'.strtoupper($ogname)] = $ogvalue;
            }
        }
        if ($this->checkSettings('opengraph')) {
            foreach ($this->ogTags as $ogname => $ogvalue) {
                if (!empty($this->SETTINGS['OG_'.strtoupper($ogname)])) {
                    $code[] = '<meta property="og:'.$ogname.'" content="'.$this->SETTINGS['OG_'.strtoupper($ogname)].'">';
                }
            }

            return implode("\n",$code);
        }
    }
    /**
     * OpenGraphAutoparse
     * Parse file or url to try and automatically set variables
     *
     *
     * @param $path url or file to parse
     *
     * @access public
     */
    public function OpenGraphAutoparse($path)
    {
        // Script calls itself or page that uses googleplus.php. Prevent infinityloop.
        if (isset($_GET['__OGAP']) && $_GET['__OGAP'] === GOOGLEPLUS_DOMAIN) {
            return false;
        }
        if (preg_match('~^https?:\/\/~',$path)) {
            if (strstr($path, GOOGLEPLUS_DOMAIN)) {
                $info = parse_url($path);
                if (isset($info['query'])) {
                    $path .= '&__OGAP='.GOOGLEPLUS_DOMAIN;
                } else {
                    $path .= '?__OGAP='.GOOGLEPLUS_DOMAIN;
                }
            }
        }
        $rawdata = file_get_contents($path);
        $doc = new \DOMDocument();
        $doc->strictErrorChecking = false;
        @$doc->loadHTML($rawdata);
        $xpath = new \DOMXPath($doc);
        $this->set('OG_TITLE',trim(fix_utf8($xpath->query('//title')->item(0)->textContent)));
        $metatags = $xpath->query('//meta'); //doc->getElementsByTagName('meta');
        foreach ($metatags as $tag) {
            if ($tag->hasAttribute('property')) { //&& strpos($tag->getAttribute('property'), 'og:') === 0) {
                $property = $tag->getAttribute('property');
            }
            if ($tag->hasAttribute('name')) {
                $property = $tag->getAttribute('name');
            }
            $property = strtolower(str_replace('og:','',$property));
            if (!array_key_exists($property, $this->ogTags)) {
                continue;
            }
            $property = 'OG_'.strtoupper($property);
            if (empty($this->SETTINGS[$property])) {
                if ($tag->hasAttribute('content')) {
                    $this->set($property,trim($tag->getAttribute('content')));
                } elseif ($tag->hasAttribute('value')) {
                    $this->set($property,trim($tag->getAttribute('value')));
                }
            }
        }
    }
    /**
     * Publisher
     * <link> tag for publisher
     *
     *
     * @return <link> tag
     *
     * @access public
     */
    public function Publisher()
    {
        if ($this->checkSettings('publisher')) {
            return '<link rel="publisher" href="'.$this->SETTINGS['ID'].'"/>';
        }
    }
    /**
     * Author
     * <link> tag for author
     *
     *
     * @return <link> tag
     *
     * @access public
     */
    public function Author()
    {
        if ($this->checkSettings('author')) {
            return '<link rel="author" href="'.$this->SETTINGS['ID'].'"/>';
        }
    }
    /**
     * cleanOutput
     * Remove uneccesary whitespaces from htmlcode
     *
     * @param $code htmlcode
     *
     * @return cleaned htmlcode
     *
     * @access private
     */
    private function cleanOutput($code)
    {
        if (is_array($code)) {
            $code = implode(' ',$code);
        }
        $code = preg_replace('~\s+~',' ',$code);
        $code = str_replace(' "','"',$code);
        $code = str_replace(' >','>',$code);

        return $code;
    }
    /**
     * JS
     * Return <script> tag to load Google+
     *
     * @param $asynchronous bool true to load javascript asynchronous
     *
     * @return <script> tag
     *
     * @access public
     */
    public function JS($asynchronous = false)
    {
        define('GOOGLEPLUS_JS_LOADED', true);
        if ($asynchronous) {
return '<script type="text/javascript">
  window.___gcfg = {
    lang: "'.$this->SETTINGS['LANG'].'",
    parsetags: "'.$this->SETTINGS['PARSETAGS'].'"
  };
  (function () {
    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
    po.src = "https://apis.google.com/js/plusone.js";
    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
  })();
</script>';
        } else {
return '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>';
        }
    }
}
